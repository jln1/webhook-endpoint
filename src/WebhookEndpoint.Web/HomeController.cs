﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.Controllers;

namespace WebhookEndpoint.Core
{
    public class HomeController : RenderController
	{
        private ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger,
			ICompositeViewEngine compositeViewEngine,
			IUmbracoContextAccessor umbracoContextAccessor)
			: base(logger, compositeViewEngine, umbracoContextAccessor)
		{
			_logger = logger;
		}
		public override IActionResult Index()
		{
			_logger.LogInformation("jj is awesome");
			return  CurrentTemplate(CurrentPage);
		}
	}

}
